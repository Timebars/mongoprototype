const User = require('./User');
const Timebarstags = require('./Timebarstags');
const Timebarsrescalcs = require('./Timebarsrescalcs');
const TimebarsRes = require('./TimebarsRes');
const Timebarsmetadata = require('./Timebarsmetadata');
const Timebarscore = require('./Timebarscore');
const Timebarsbaseline = require('./Timebarsbaseline');
const Timebarsadminpanel = require('./Timebarsadminpanel');
const Timebarsdocs = require('./Timebarsdocs');

module.exports = {
  User,
  Timebarstags,
  Timebarsrescalcs,
  TimebarsRes,
  Timebarsmetadata,
  Timebarscore,
  Timebarsbaseline,
  Timebarsadminpanel,
  Timebarsdocs
}
