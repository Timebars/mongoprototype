const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tbDocumentsSchema = new Schema({
  tbDocID: {
    type: String,
    required: true,
    unique: true,
  },
  tbDocName: {
    type: String,
    required: true,
  },
  tbDocCustomerID: {
    type: String,
    required: true,
  },
  tbDocBriefDescription: {
    type: String,
    required: true,
  },
  tbDocType: {
    type: String,
    required: true,
  },
  tbDocNameShort: String,
  tbDocHTMLContent: String,
  tbDocGroup: String,
  tbDocPurpose: String,
  tbDocOwner: String,
  tbDocPopular: String,
  tbDocSortOrder: String,
  tbDocLastModified: String,
  tbDocApprovedBy: String,
  tbDocStatus: String,
  tbDocWrittenBy: String,
  tbDocL1: String,
  tbDocL2: String,
  tbDocL3: String,
  tbDocL4: String,
  tbDocL5: String,
  tbDocCardImage: String
});

module.exports = mongoose.model('tbdocuments', tbDocumentsSchema);
