const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const tbTagsSchema = new Schema({

    tbTagID: {
        type: String,
        unique: true,
        required: true
    },
    tbTagCustomerID: String,
    tbTagAzureID: String,
    tbTagGroup: String,
    tbTagName: String,
    tbTagNameShort: String,
    tbTagPurpose: String,
    tbTagDescription: String,
    tbTagOwner: String,
    tbTagPopular: String,
    tbTagSortOrder: String,
    tbTagLastModified: String,
    tbTagEntity: String
});

// note tbtags is the collection name
module.exports = mongoose.model('tbtags', tbTagsSchema);
