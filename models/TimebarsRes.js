const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema
const tbResSchema = new Schema({

    tbResID: {
        type: String,
        unique: true,
        required: true
    },
    tbResCustomerID: String,
    tbResAzureID: String,
    tbResName: String,
    tbResNameShort: String,
    tbResPrimarySkill: String,
    tbResPrimaryRole: String,
    tbResDepartment: String,
    tbResManager: String,
    tbResPayRate: String,
    tbResCoordTop: String,
    tbResCoordLeft: String,
    tbResTeam: String,
    tbResLocation: String,
    tbResCostCode: String,
    tbResTeamLeader: String,
    tbResSupervisor: String,
    tbResPartTimeFullTime: String,
    tbResResourceType: String,
    tbResResourceClass: String,
    tbResResourceCalendar: String,
    tbResPercentGeneralAvailability: String,
    tbResExtSystemResID: String,
    tbResSortOrder: String,
    tbResLastModified: String,
    tbResLabourType: String,
    tbResEmail: String,
    tbResPasswordHash: String,
    tbResPIN: String
});

// note tbresources is the collection name
module.exports = mongoose.model('tbresources', tbResSchema);
