const express = require('express');
const advancedResults = require('../middleware/advancedResults');
const { Timebarsadminpanel } = require('../models');
const {
	createTbAdminpanel,
	getTbAdminpanels,
	deleteTbAdminpanels,
	getTbAdminpanel,
	updateTbAdminpanel,
	deleteTbAdminpanel,
  getTbAdminpanelByApid,
  deleteTbAdminpanelByApid
} = require('../controllers/timebarsadminpanel');
const { protect } = require('../middleware/auth');

const router = express.Router();


router
	.route('/')
	.post(protect, createTbAdminpanel)
	.delete(protect, deleteTbAdminpanels)
	.get(advancedResults(Timebarsadminpanel), getTbAdminpanels);

router
	.route('/:id')
	.get(getTbAdminpanel)
	.put(updateTbAdminpanel)
	.delete(protect, deleteTbAdminpanel);

router
	.route('/apid/:apid')
	.get(getTbAdminpanelByApid)
	.delete(protect, deleteTbAdminpanelByApid);

module.exports = router;
