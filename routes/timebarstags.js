const express = require('express');
const advancedResults = require('../middleware/advancedResults');
const { Timebarstags } = require('../models');
const {
	createTbTags,
	getTbTags,
	deleteTbTags,
	getTbTag,
	updateTbTag,
	deleteTbTag,
  getTbTagByTbtagid,
  deleteTbTagByTbtagid
} = require('../controllers/timebarstags');
const { protect } = require('../middleware/auth');

const router = express.Router();


router
	.route('/')
	.post(protect, createTbTags)
	.delete(protect, deleteTbTags)
	.get(advancedResults(Timebarstags), getTbTags);

router
	.route('/:id')
	.get(getTbTag)
	.put(updateTbTag)
	.delete(protect, deleteTbTag);

router
	.route('/tbtagid/:tbtagid')
	.get(getTbTagByTbtagid)
	.delete(protect, deleteTbTagByTbtagid);

module.exports = router;
