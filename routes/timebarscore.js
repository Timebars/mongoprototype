const express = require('express');
const advancedResults = require('../middleware/advancedResults');
const TbCoreModel = require('../models/Timebarscore');
const {
    createTbCore,
    getTbCores,
    deleteTbcores,
    getTbCore,
    updateTbCore,
    deleteTbcore,
    getTbCoreByTbid,
    deleteTbCoreByTbid
} = require('../controllers/timebarscore');
const { protect } = require('../middleware/auth');

const router = express.Router();

// @route   GET /tbcore/test
// @desc    Tests Timebarscore route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Timebartbcore route Works' }));

router
    .route('/')
    .post(protect, createTbCore)
    .delete(protect, deleteTbcores)
    .get(advancedResults(TbCoreModel), getTbCores);

router
    .route('/:id')
    .get(getTbCore)
    .put(updateTbCore)
    .delete(protect, deleteTbcore);

router
    .route('/tbid/:tbid')
    .get(getTbCoreByTbid)
    .delete(protect, deleteTbCoreByTbid);

module.exports = router;
