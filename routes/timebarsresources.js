const express = require('express');
const advancedResults = require('../middleware/advancedResults');
const { TimebarsRes } = require('../models');
const {
	createTbResources,
	getTbResources,
	deleteTbResources,
	getTbResource,
	updateTbResource,
	deleteTbResource,
  getTbResByTbResid,
  deleteTbResByTbResid
} = require('../controllers/timebarsresources');
const { protect } = require('../middleware/auth');

const router = express.Router();


router
	.route('/')
	.post(protect, createTbResources)
	.delete(protect, deleteTbResources)
	.get(advancedResults(TimebarsRes), getTbResources);

router
	.route('/:id')
	.get(getTbResource)
	.put(updateTbResource)
	.delete(protect, deleteTbResource);

router
	.route('/tbresid/:tbresid')
	.get(getTbResByTbResid)
	.delete(protect, deleteTbResByTbResid);

module.exports = router;
