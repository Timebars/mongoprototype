const asyncHandler = require('../middleware/async');
const { Timebarsbaseline } = require('../models');
const ErrorResponse = require('../utils/errorResponse');

// @route   POST /api/v1/tbbaseline/   
// @desc    Create tbbaseline
// @access  Private
exports.createTbBaseline = asyncHandler( async (req, res) => {
  const {
    tbID,
    tbCustomerID,
    tbAzureID,
    tbName,
    tbL1,
    tbL2,
    tbL3,
    tbL4,
    tbL5,
    tbDuration,
    tbRemainingDuration,
    tbStart,
    tbFinish,
    tbOwner,
    tbCoordTop,
    tbCoordLeft,
    tbCoordRight,
    tbType,
    tbResID,
    tbMetaDataID,
    tbBLID,
    tbCalendar,
    tbPredecessor,
    tbAStart,
    tbAFinish,
    tbWork,
    tbAWork,
    tbWorkRemaining,
    tbPercentComplete,
    tbConstraintType,
    tbConstraintDate,
    tbFloat,
    tbFreeFloat,
    tbPercentTimeOn,
    tbExpHoursPerWeek,
    tbCostID,
    tbCost,
    tbCostRemaining,
    tbACost,
    tbPayRate,
    tbCostType,
    tbSelfKey2,
    tbBarColor,
    tbTextColor,
    kbCoordLeft,
    kbCoordTop,
    canvasNo,
    focdItemCoordLeft,
    focdItemCoordTop,
    tbHierarchyOrder
  } = req.body;

  const newBaseline = await Timebarsbaseline.create({
    tbID,
    tbCustomerID,
    tbAzureID,
    tbName,
    tbL1,
    tbL2,
    tbL3,
    tbL4,
    tbL5,
    tbDuration,
    tbRemainingDuration,
    tbStart,
    tbFinish,
    tbOwner,
    tbCoordTop,
    tbCoordLeft,
    tbCoordRight,
    tbType,
    tbResID,
    tbMetaDataID,
    tbBLID,
    tbCalendar,
    tbPredecessor,
    tbAStart,
    tbAFinish,
    tbWork,
    tbAWork,
    tbWorkRemaining,
    tbPercentComplete,
    tbConstraintType,
    tbConstraintDate,
    tbFloat,
    tbFreeFloat,
    tbPercentTimeOn,
    tbExpHoursPerWeek,
    tbCostID,
    tbCost,
    tbCostRemaining,
    tbACost,
    tbPayRate,
    tbCostType,
    tbSelfKey2,
    tbBarColor,
    tbTextColor,
    kbCoordLeft,
    kbCoordTop,
    canvasNo,
    focdItemCoordLeft,
    focdItemCoordTop,
    tbHierarchyOrder
  });

  res.status(201).json({
    success: true,
    data: newBaseline
  });
});

// @route   GET /api/v1/tbbaseline/
// @desc    Get all tbbaseline 
// @access  Public
exports.getTbBaselines = asyncHandler( async (req, res) => {
  res.json(res.advancedResults);
});

// @route DELETE /api/v1/tbbaseline/
// @desc DELETE all tbbaseline
// @access Private
exports.deleteTbBaselines = asyncHandler( async (req, res, next) => {
  const deletedTbBaselines = await Timebarsbaseline.deleteMany();

  if (!deletedTbBaselines.deletedCount) return next(new ErrorResponse('No tbbaseline found', 404));

  res.json({
    success: true,
    data: {}
  })
});

// @route   GET /api/v1/tbbaseline/:id
// @desc    Get one tbbaseline by id
// @access  Public
exports.getTbBaseline = asyncHandler( async (req, res, next) => {
  const tbBaseline = await Timebarsbaseline.findById(req.params.id);

  if (!tbBaseline) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: tbBaseline
  });
});

// @route   PUT /api/v1/tbbaseline/:id
// @desc    Update on one tbbaseline by id
// @access  Public
exports.updateTbBaseline = asyncHandler( async (req, res, next) => {
  const updatedTbBaseline = await Timebarsbaseline.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  if (!updatedTbBaseline) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: updatedTbBaseline
  });
});

// @route   DELETE /api/v1/tbbaseline/:id
// @desc    Delete tbbaseline by id
// @access  Private
exports.deleteTbBaseline = asyncHandler( async (req, res, next) => {
  const deletedTbBaseline = await Timebarsbaseline.findByIdAndDelete(req.params.id);

  if (!deletedTbBaseline) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: {}
  });
});

// @route GET /api/v1/tbbaseline/tbid/:tbid
// @desc Get tbBaseline by tbID
// @access  Public
exports.getTbBaselineByTbid = asyncHandler( async (req, res, next) => {
  const tbBaseline = await Timebarsbaseline.findOne({ tbID: req.params.tbid });

  if (!tbBaseline) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: tbBaseline
  });
});

// @route DELETE /api/v1/tbbaseline/tbid/:tbid
// @desc DELETE tbBaseline by tbID
// @access  Private
exports.deleteTbBaselineBytbid = asyncHandler( async (req, res, next) => {
  const deletedTbBaseline = await Timebarsbaseline.findOneAndDelete({ tbID: req.params.tbid });

  if (!deletedTbBaseline) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: {}
  });
});
