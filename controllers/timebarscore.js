const asyncHandler = require('../middleware/async');
const TbCoreModel = require('../models/Timebarscore');
const ErrorResponse = require('../utils/errorResponse');

// @route   POST /api/v1/tbcore/   
// @desc    Create tbcore
// @access  Private
exports.createTbCore = asyncHandler(async (req, res) => {
  const { tbID, tbName, tbType, tbStart } = req.body;
  const newCore = await TbCoreModel.create({
    tbID, tbName, tbType, tbStart
  });

  res.status(201).json({
    success: true,
    data: newCore
  });
});

// @route   GET /api/v1/tbcore/
// @desc    Get all tbcore 
// @access  Public
exports.getTbCores = asyncHandler(async (req, res) => {
  res.json(res.advancedResults);
});

// @route DELETE /api/v1/tbcore
// @desc Delete all tbcore
// @access Private
exports.deleteTbcores = asyncHandler(async (req, res, next) => {
  const deletedTbCores = await TbCoreModel.deleteMany();

  if (!deletedTbCores.deletedCount) return next(new ErrorResponse('No tbcores found', 404));

  res.json({
    success: true,
    data: {}
  });
});

// @route   GET /api/v1/tbcore/:id
// @desc    Get one tbcore by id
// @access  Public
exports.getTbCore = asyncHandler(async (req, res, next) => {
  const tbCore = await TbCoreModel.findById(req.params.id);

  if (!tbCore) return next (new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: tbCore
  });
});

// @route   GET /api/v1/tbcore/:id
// @desc    Update on one tbcore by id
// @access  Public
exports.updateTbCore = asyncHandler(async (req, res, next) => {
  const updatedTbCore = await TbCoreModel.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  if (!updatedTbCore) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: updatedTbCore
  });
});

// @route   DELETE /api/v1/tbcore/:id
// @desc    Delete tbcore by id
// @access   Private
exports.deleteTbcore = asyncHandler(async (req, res, next) => {
  const deletedTbCore = await TbCoreModel.findByIdAndDelete(req.params.id);

  if (!deletedTbCore) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: {}
  });
});

// @route GET /api/v1/tbcore/tbid/:tbid
// @desc Get tbcore by tbid
// @access public
exports.getTbCoreByTbid = asyncHandler(async (req, res, next) => {
  const tbCore = await TbCoreModel.findOne({ tbID: req.params.tbid });

  if (!tbCore) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: tbCore
  });
});

// @route DELETE /api/v1/tbcore/tbid/:tbid
// @desc Delete tbcore by tbid
// @access Private
exports.deleteTbCoreByTbid = asyncHandler(async (req, res, next) => {
  const deleteTbcore = await TbCoreModel.findOneAndDelete({ tbID: req.params.tbid });
  
  if (!deleteTbcore) return next(new ErrorResponse('Resource not found', 404));
  
  res.json({
    success: true,
    data: {}
  });
});
