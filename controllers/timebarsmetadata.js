const asyncHandler = require('../middleware/async');
const { Timebarsmetadata } = require('../models');
const ErrorResponse = require('../utils/errorResponse');

// @route   POST /api/v1/tbmetadata/   
// @desc    Create tbmetadata
// @access  Private
exports.createTbMetadata = asyncHandler( async (req, res) => {
  const {
    tbMDID,
    tbMDGate,
    tbMDHealthOverall,
    tbMDHealthScope,
    tbMDHealthCost,
    tbMDHealthIssues,
    tbMDHealthRisk,
    tbMDHealthSchedule,
    tbMDHealthHours,
    tbMDInvestmentCategory,
    tbMDInvestmentInitiative,
    tbMDInvestmentObjective,
    tbMDInvestmentStrategy,
    tbMDROMEstimate,
    tbMDPortfolio,
    tbMDProgram,
    tbMDProgActivityAlignment,
    tbMDSize,
    tbMDStageApprover,
    tbMDWrittenBy,
    tbMDBusinessAdvisor,
    tbMDBusinessOwner,
    tbMDDeliveryManager,
    tbMDOrgManager,
    tbMDPriorityStrategic,
    tbMDSponsoringDepartment,
    tbMDPrimaryContact,
    tbMDBenefitCostRatio,
    tbMDContactNumber,
    tbMDResponsibleTeam,
    tbMDRiskVsSizeAndComplexity,
    tbMDEcnomicValueAdded,
    tbMDEstimationClass,
    tbMDInternalRateOfReturn,
    tbMDSprintName,
    tbMDSunkCosts,
    tbMDSyncNotes,
    tbMDNotesProject,
    tbMDContractNumber,
    tbMDNetPresentValue,
    tbMDOpportunityCost,
    tbMDPaybackPeriod,
    tbMDPrimaryLineOfBusiness,
    tbMDBackgroundInfo,
    tbMDCapabilitiesNeeded,
    tbMDConsequence,
    tbMDExpectedBenfits,
    tbMDProblemOpportunity,
    tbMDConstraintsAssumptions,
    tbMDCostBenefitAnalysis,
    tbMDExecutiveSummary,
    tbMDSeniorLevelCommittment,
    tbMDStakeholderDescription,
    tbMDNotesWorkflow,
    tbMDOther5,
    tbMDOther6,
    tbMDOther7,
    tbMDOther8,
    tbMDOther9,
    tbMDOther10,
    tbMDOther11,
    tbMDOther12,
    tbMDName,
    tbMDCustomerID,
    tbMDAzureID,
    tbMDNameShort,
    tbMDProjectNumber,
    tbMDDescription,
    tbMDNotes,
    tbMDExtLink1,
    tbMDExtSystemID1,
    tbMDSortOrder,
    tbMDtbLastModified,
    tbMDPriority,
    tbMDStatus,
    tbMDState,
    tbMDSeverity,
    tbMDStage,
    tbMDPhase,
    tbMDCategory,
    tbMDHealth,
    tbMDResponsibility,
    tbMDDepartment,
    tbMDExSponsor,
    tbMDPM,
    tbMDProjectType,
    tbMDShowIn,
    tbMDYesNoSelector,
    tbMDProduct,
    tbMDContact,
    tbMDWBS,
    tbMDWeighting,
    tbMDLocation,
    tbMDPrimarySkill,
    tbMDPrimaryRole,
    tbMDOther2,
    tbMDOther3,
    tbMDOther4,
    canvasNo,
    tbMDRefID
  } = req.body;

  const newMetadata = await Timebarsmetadata.create({
    tbMDID,
    tbMDGate,
    tbMDHealthOverall,
    tbMDHealthScope,
    tbMDHealthCost,
    tbMDHealthIssues,
    tbMDHealthRisk,
    tbMDHealthSchedule,
    tbMDHealthHours,
    tbMDInvestmentCategory,
    tbMDInvestmentInitiative,
    tbMDInvestmentObjective,
    tbMDInvestmentStrategy,
    tbMDROMEstimate,
    tbMDPortfolio,
    tbMDProgram,
    tbMDProgActivityAlignment,
    tbMDSize,
    tbMDStageApprover,
    tbMDWrittenBy,
    tbMDBusinessAdvisor,
    tbMDBusinessOwner,
    tbMDDeliveryManager,
    tbMDOrgManager,
    tbMDPriorityStrategic,
    tbMDSponsoringDepartment,
    tbMDPrimaryContact,
    tbMDBenefitCostRatio,
    tbMDContactNumber,
    tbMDResponsibleTeam,
    tbMDRiskVsSizeAndComplexity,
    tbMDEcnomicValueAdded,
    tbMDEstimationClass,
    tbMDInternalRateOfReturn,
    tbMDSprintName,
    tbMDSunkCosts,
    tbMDSyncNotes,
    tbMDNotesProject,
    tbMDContractNumber,
    tbMDNetPresentValue,
    tbMDOpportunityCost,
    tbMDPaybackPeriod,
    tbMDPrimaryLineOfBusiness,
    tbMDBackgroundInfo,
    tbMDCapabilitiesNeeded,
    tbMDConsequence,
    tbMDExpectedBenfits,
    tbMDProblemOpportunity,
    tbMDConstraintsAssumptions,
    tbMDCostBenefitAnalysis,
    tbMDExecutiveSummary,
    tbMDSeniorLevelCommittment,
    tbMDStakeholderDescription,
    tbMDNotesWorkflow,
    tbMDOther5,
    tbMDOther6,
    tbMDOther7,
    tbMDOther8,
    tbMDOther9,
    tbMDOther10,
    tbMDOther11,
    tbMDOther12,
    tbMDName,
    tbMDCustomerID,
    tbMDAzureID,
    tbMDNameShort,
    tbMDProjectNumber,
    tbMDDescription,
    tbMDNotes,
    tbMDExtLink1,
    tbMDExtSystemID1,
    tbMDSortOrder,
    tbMDtbLastModified,
    tbMDPriority,
    tbMDStatus,
    tbMDState,
    tbMDSeverity,
    tbMDStage,
    tbMDPhase,
    tbMDCategory,
    tbMDHealth,
    tbMDResponsibility,
    tbMDDepartment,
    tbMDExSponsor,
    tbMDPM,
    tbMDProjectType,
    tbMDShowIn,
    tbMDYesNoSelector,
    tbMDProduct,
    tbMDContact,
    tbMDWBS,
    tbMDWeighting,
    tbMDLocation,
    tbMDPrimarySkill,
    tbMDPrimaryRole,
    tbMDOther2,
    tbMDOther3,
    tbMDOther4,
    canvasNo,
    tbMDRefID
  });

  res.status(201).json({
    success: true,
    data: newMetadata
  });
});

// @route   GET /api/v1/tbmetadata/
// @desc    Get all tbmetadata 
// @access  Public
exports.getTbMetadatas = asyncHandler( async (req, res) => {
  res.json(res.advancedResults);
});

// @route DELETE /api/v1/tbmetadata/
// @desc DELETE all tbmetadata
// @access Private
exports.deleteTbMetadatas = asyncHandler( async (req, res, next) => {
  const deletedTbMetadatas = await Timebarsmetadata.deleteMany();

  if (!deletedTbMetadatas.deletedCount) return next(new ErrorResponse('No tbMetadata found', 404));

  res.json({
    success: true,
    data: {}
  })
});

// @route   GET /api/v1/tbmetadata/:id
// @desc    Get one tbmetadata by id
// @access  Public
exports.getTbMetadata = asyncHandler( async (req, res, next) => {
  const tbMetadata = await Timebarsmetadata.findById(req.params.id);

  if (!tbMetadata) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: tbMetadata
  });
});

// @route   PUT /api/v1/tbmetadata/:id
// @desc    Update on one tbmetadata by id
// @access  Public
exports.updateTbMetadata = asyncHandler( async (req, res, next) => {
  const updatedTbMetadata = await Timebarsmetadata.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  if (!updatedTbMetadata) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: updatedTbMetadata
  });
});

// @route   DELETE /api/v1/tbmetadata/:id
// @desc    Delete tbmetadata by id
// @access  Private
exports.deleteTbMetadata = asyncHandler( async (req, res, next) => {
  const deletedTbMetadata = await Timebarsmetadata.findByIdAndDelete(req.params.id);

  if (!deletedTbMetadata) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: {}
  });
});

// @route GET /api/v1/tbmetadata/tbmdid/:tbmdid
// @desc Get tbMetadata by tbMDID
// @access  Public
exports.getTbMetadataByTbMdid = asyncHandler( async (req, res, next) => {
  const tbMetadata = await Timebarsmetadata.findOne({ tbMDID: req.params.tbmdid });

  if (!tbMetadata) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: tbMetadata
  });
});

// @route DELETE /api/v1/tbmetadata/tbmdid/:tbmdid
// @desc DELETE tbMetadata by tbMDID
// @access  Private
exports.deleteTbMetadataBytbMdid = asyncHandler( async (req, res, next) => {
  const deletedTbMetadata = await Timebarsmetadata.findOneAndDelete({ tbMDID: req.params.tbmdid });

  if (!deletedTbMetadata) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: {}
  });
});
