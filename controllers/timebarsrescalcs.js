const asyncHandler = require('../middleware/async');
const { Timebarsrescalcs } = require('../models');
const ErrorResponse = require('../utils/errorResponse');

// @route   POST /api/v1/tbrescalcs/   
// @desc    Create tbrescalcs
// @access  Private
exports.createTbRescalc = asyncHandler( async (req, res) => {
  const {
    tbResCalcID,
    tbResCalcTbResID,
    tbResCalcTbID,
    tbResCalcName,
    tbResCalcWeek,
    tbResCalcHours,
    tbResCalcWdays,
    tbResCalcStartWeekNo,
    tbResCalcTbWeekNo,
    tbResCalcWeekSumHours,
    tbResCalcWeekSumCost,
    tbResCalcWeek1,
    tbResCalcWeek2,
    tbResCalcWeek3,
    tbResCalcWeek4,
    tbResCalcWeek5,
    tbResCalcWeek6,
    tbResCalcWeek7,
    tbResCalcWeek8,
    tbResCalcWeek9,
    tbResCalcWeek10,
    tbResCalcWeek11,
    tbResCalcWeek12,
    tbResCalcWeek13,
    tbResCalcWeek14,
    tbResCalcWeek15,
    tbResCalcWeek16,
    tbResCalcWeek17,
    tbResCalcWeek18,
    tbResCalcWeek19,
    tbResCalcWeek20,
    tbResCalcWeek21,
    tbResCalcWeek22,
    tbResCalcWeek23,
    tbResCalcWeek24,
    tbResCalcWeek25,
    tbResCalcWeek26,
    tbResCalcWeek27,
    tbResCalcWeek28,
    tbResCalcWeek29,
    tbResCalcWeek30,
    tbResCalcWeek31,
    tbResCalcWeek32,
    tbResCalcAzureID,
    tbResCalcCustomerID,
    tbResCalcLastModified
  } = req.body;

  const newRescalcs = await Timebarsrescalcs.create({
    tbResCalcID,
    tbResCalcTbResID,
    tbResCalcTbID,
    tbResCalcName,
    tbResCalcWeek,
    tbResCalcHours,
    tbResCalcWdays,
    tbResCalcStartWeekNo,
    tbResCalcTbWeekNo,
    tbResCalcWeekSumHours,
    tbResCalcWeekSumCost,
    tbResCalcWeek1,
    tbResCalcWeek2,
    tbResCalcWeek3,
    tbResCalcWeek4,
    tbResCalcWeek5,
    tbResCalcWeek6,
    tbResCalcWeek7,
    tbResCalcWeek8,
    tbResCalcWeek9,
    tbResCalcWeek10,
    tbResCalcWeek11,
    tbResCalcWeek12,
    tbResCalcWeek13,
    tbResCalcWeek14,
    tbResCalcWeek15,
    tbResCalcWeek16,
    tbResCalcWeek17,
    tbResCalcWeek18,
    tbResCalcWeek19,
    tbResCalcWeek20,
    tbResCalcWeek21,
    tbResCalcWeek22,
    tbResCalcWeek23,
    tbResCalcWeek24,
    tbResCalcWeek25,
    tbResCalcWeek26,
    tbResCalcWeek27,
    tbResCalcWeek28,
    tbResCalcWeek29,
    tbResCalcWeek30,
    tbResCalcWeek31,
    tbResCalcWeek32,
    tbResCalcAzureID,
    tbResCalcCustomerID,
    tbResCalcLastModified
  });

  res.status(201).json({
    success: true,
    data: newRescalcs
  });
});

// @route   GET /api/v1/tbrescalcs/
// @desc    Get all tbrescalcs 
// @access  Public
exports.getTbRescalcs = asyncHandler( async (req, res) => {
  res.json(res.advancedResults);
});

// @route DELETE /api/v1/tbrescalcs/
// @desc DELETE all tbrescalcs
// @access Private
exports.deleteTbRescalcs = asyncHandler( async (req, res, next) => {
  const deletedTbRescalcs = await Timebarsrescalcs.deleteMany();

  if (!deletedTbRescalcs.deletedCount) return next(new ErrorResponse('No tbtags found', 404));

  res.json({
    success: true,
    data: {}
  })
});

// @route   GET /api/v1/tbrescalcs/:id
// @desc    Get one tbrescalcs by id
// @access  Public
exports.getTbRescalc = asyncHandler( async (req, res, next) => {
  const tbRescalc = await Timebarsrescalcs.findById(req.params.id);

  if (!tbRescalc) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: tbRescalc
  });
});

// @route   PUT /api/v1/tbrescalcs/:id
// @desc    Update on one tbrescalcs by id
// @access  Public
exports.updateTbRescalc = asyncHandler( async (req, res, next) => {
  const updatedTbRescalc = await Timebarsrescalcs.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  if (!updatedTbRescalc) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: updatedTbRescalc
  });
});

// @route   DELETE /api/v1/tbrescalcs/:id
// @desc    Delete tbrescalcs by id
// @access  Private
exports.deleteTbRescalc = asyncHandler( async (req, res, next) => {
  const deletedTbRescalc = await Timebarsrescalcs.findByIdAndDelete(req.params.id);

  if (!deletedTbRescalc) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: {}
  });
});

// @route GET /api/v1/tbrescalcs/tbrescalcid/:tbresclacid
// @desc Get tbRescalc by tbResCalcID
// @access  Public
exports.getTbRescalcByTbrescalcid = asyncHandler( async (req, res, next) => {
  const tbRescalc = await Timebarsrescalcs.findOne({ tbResCalcID: req.params.tbrescalcid });

  if (!tbRescalc) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: tbRescalc
  });
});

// @route DELETE /api/v1/tbrescalcs/tbresclacid/:tbresclacid
// @desc DELETE tbrescalcs by tbResCalcID
// @access  Private
exports.deleteTbRescalcByTbresclacid = asyncHandler( async (req, res, next) => {
  const deletedTbRescalc = await Timebarsrescalcs.findOneAndDelete({ tbResCalcID: req.params.tbrescalcid });

  if (!deletedTbRescalc) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: {}
  });
});
