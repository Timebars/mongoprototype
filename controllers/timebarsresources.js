const asyncHandler = require('../middleware/async');
const { TimebarsRes } = require('../models');
const ErrorResponse = require('../utils/errorResponse');

// @route   POST /api/v1/tbresources/   
// @desc    Create tbresources
// @access  Private
exports.createTbResources = asyncHandler( async (req, res) => {
  const {
    tbResID,
    tbResCustomerID,
    tbResAzureID,
    tbResName,
    tbResNameShort,
    tbResPrimarySkill,
    tbResPrimaryRole,
    tbResDepartment,
    tbResManager,
    tbResPayRate,
    tbResCoordTop,
    tbResCoordLeft,
    tbResTeam,
    tbResLocation,
    tbResCostCode,
    tbResTeamLeader,
    tbResSupervisor,
    tbResPartTimeFullTime,
    tbResResourceType,
    tbResResourceClass,
    tbResResourceCalendar,
    tbResPercentGeneralAvailability,
    tbResExtSystemResID,
    tbResSortOrder,
    tbResLastModified,
    tbResLabourType,
    tbResEmail,
    tbResPasswordHash,
    tbResPIN
  } = req.body;

  const newRes = await TimebarsRes.create({
    tbResID,
    tbResCustomerID,
    tbResAzureID,
    tbResName,
    tbResNameShort,
    tbResPrimarySkill,
    tbResPrimaryRole,
    tbResDepartment,
    tbResManager,
    tbResPayRate,
    tbResCoordTop,
    tbResCoordLeft,
    tbResTeam,
    tbResLocation,
    tbResCostCode,
    tbResTeamLeader,
    tbResSupervisor,
    tbResPartTimeFullTime,
    tbResResourceType,
    tbResResourceClass,
    tbResResourceCalendar,
    tbResPercentGeneralAvailability,
    tbResExtSystemResID,
    tbResSortOrder,
    tbResLastModified,
    tbResLabourType,
    tbResEmail,
    tbResPasswordHash,
    tbResPIN
  });

  res.status(201).json({
    success: true,
    data: newRes
  });
});

// @route   GET /api/v1/tbresources/
// @desc    Get all tbresources 
// @access  Public
exports.getTbResources = asyncHandler( async (req, res) => {
  res.json(res.advancedResults);
});

// @route DELETE /api/v1/tbresources/
// @desc DELETE all tbresources
// @access Private
exports.deleteTbResources = asyncHandler( async (req, res, next) => {
  const deletedTbResources = await TimebarsRes.deleteMany();

  if (!deletedTbResources.deletedCount) return next(new ErrorResponse('No tbtags found', 404));

  res.json({
    success: true,
    data: {}
  })
});

// @route   GET /api/v1/tbresources/:id
// @desc    Get one tbresources by id
// @access  Public
exports.getTbResource = asyncHandler( async (req, res, next) => {
  const tbResource = await TimebarsRes.findById(req.params.id);

  if (!tbResource) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: tbResource
  });
});

// @route   PUT /api/v1/tbresources/:id
// @desc    Update on one tbresources by id
// @access  Public
exports.updateTbResource = asyncHandler( async (req, res, next) => {
  const updatedTbResource = await TimebarsRes.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  if (!updatedTbResource) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: updatedTbResource
  });
});

// @route   DELETE /api/v1/tbresources/:id
// @desc    Delete tbresources by id
// @access  Private
exports.deleteTbResource = asyncHandler( async (req, res, next) => {
  const deletedTbResource = await TimebarsRes.findByIdAndDelete(req.params.id);

  if (!deletedTbResource) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: {}
  });
});

// @route GET /api/v1/tbresources/tbresid/:tbresid
// @desc Get tbresources by tbResID
// @access  Public
exports.getTbResByTbResid = asyncHandler( async (req, res, next) => {
  const tbResource = await TimebarsRes.findOne({ tbResID: req.params.tbresid });

  if (!tbResource) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: tbResource
  });
});

// @route DELETE /api/v1/tbresources/tbresid/:tbresid
// @desc DELETE tbresource by tbResID
// @access  Private
exports.deleteTbResByTbResid = asyncHandler( async (req, res, next) => {
  const deletedTbResource = await TimebarsRes.findOneAndDelete({ tbResID: req.params.tbresid });

  if (!deletedTbResource) return next(new ErrorResponse('Resource not found', 404));

  res.json({
    success: true,
    data: {}
  });
});
