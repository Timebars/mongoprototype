
// my web server

var express = require('express');
var app = express();
// var bodyParser = require('body-parser');
var currentId = 2;
var PORT = process.env.PORT || 3001;

// seems to allow any html file to render
app.use(express.static(__dirname));


app.use(express.json());

// set up db connecton
const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017';

const fetch = require('node-fetch');

// Make a connection to MongoDB Service
MongoClient.connect(url, { useNewUrlParser: true }, function (err, client) {
    if (err) throw err;


    const db = client.db('timebars');
    const collection = db.collection('products');

    console.log(`Connected to MongoDB on  url: ${url}`);

    client.close();
});

console.log(`Directory location: ${__dirname}`)


const schengResults = schedulingEngine("20 dec", "25 dec", "7.5")

app.get('/schengine', function (req, res) {


    res.send({
        schengResults: schengResults


    });


});

// in node now... take params and run through node sch engine
function schedulingEngine(start, finish, shift) {

    // ... this is the 100's line of sched engine code, it crunches input params
    var newStart = "S 1 March"
    var newFinish = "F 12 March"
    var newShift = "99"

    // and returns the new calculations
    var newSchCalcs = {
        "start": newStart,
        "finish": newFinish,
        "shift": newShift
    }

    return newSchCalcs
}



app.get('/schengine2', function (req, res) {

    res.send({

        task_names: task_names,


    });

});



// receive from client side, sch engine json of paramaters e.g. start finish shift
app.post('/schengine', function (req, res) {

    var start = req.body.start;
    var finish = req.body.finish;
    var shift = req.body.shift;

    // invoke scheduling engine with params from client
    const schengResults = schedulingEngine(start, finish, shift)

    // send the new calcs back to clinet
    res.send(schengResults);
});



