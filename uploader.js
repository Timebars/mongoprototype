
const express = require('express');
const router = express.Router();
const keys = require('../../config/keys');


// added April 3 for csv upload and processing
const http = require('http');
const fs = require('fs');
const multer = require('multer');
const path = require('path');
const Sharp = require('sharp');

// ??

// @route   GET api/uploadfile/test ///
// @desc    Tests uploadfile route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'uploadfile route Works' }));


// @route   POST api/uploadmultiple
// @desc    UUploading multiple files
// @access  Private




// Images
// sample using sharp to resise the image into three sizes and store original
// would need calls to build gui to view the files, delete files, upload a replace etc.

let originalsPath = './public/uploads/images/originals/';
let smallImagesPath = './public/uploads/images/small/';
let mediumImagesPath = './public/uploads/images/medium/';
let largeImagesPath = './public/uploads/images/large/';

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, originalsPath)
  },
  filename: function (req, file, cb) {
    let extension = file.originalname.split(".");
    cb(null, Date.now() + '.' + extension[extension.length - 1])
  }
});

//let upload = multer({ storage: storage });

const uploadimages = multer({
  storage: storage,
  limits: { fileSize: 1000000 },
  fileFilter: function (req, file, cb) {
    checkImageFileType(file, cb);
  }
}).single('img');


// @route   GET api/upload/image/small ///
// @desc    Upload image
// @access  Private 

router.post('/image/small', (req, res) => {

  // console.log("did this")

  uploadimages(req, res, (err) => {
    if (err) {
      res.render('index', {
        msg: err
      });
    } else {
      if (req.file == undefined) {
        res.render('index', {
          msg: 'Error: No File Selected!'
        });
      } else {

        // send resized file
        let img_dest = smallImagesPath + req.file.filename;
        Sharp(originalsPath + req.file.filename)
          .jpeg({ quality: 50 })
          .webp({ quality: 50 })
          .png({ quality: 50 })
          .toFile(img_dest, function (err) {
            //  res.send(err);

            res.render('index1', {
              msg: 'File Uploaded!',
              file: `${originalsPath}${req.file.filename}`
            });
          });
      }
    }
  });
});


// Check image File Type
function checkImageFileType(file, cb) {
  // Allowed ext
  const filetypes = /jpeg|jpg|png|gif/;
  // Check extension
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  // Check mime
  const mimetype = filetypes.test(file.mimetype);

  if (mimetype && extname) {
    return cb(null, true);
  } else {
    cb('Error: Images Only!');
  }
}

// @route   GET api/upload/images ///
// @desc    Tests upload images route  
// @access  Private

router.post('/image', (req, res) => {
  uplimages(req, res, (err) => {
    if (err) {
      res.render('index', {
        msg: err
      });
    } else {
      if (req.file == undefined) {
        res.render('index', {
          msg: 'Error: No File Selected!'
        });
      } else {
        res.render('index', {
          msg: 'File Uploaded!',
          file: `uploads/images${req.file.filename}`
        });
      }
    }
  });
});



/* start text file upload 
uploads/jsonfiles
uploads/csvfiles
uploads/jsfiles
*/
// Set The Storage Engine
const storageJSONFiles = multer.diskStorage({
  destination: './public/uploads/jsonfiles',
  filename: function (req, file, cb) {
    // cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    cb(null, file.originalname);
  }
});

// Init json file Upload
const upljson = multer({
  storage: storageJSONFiles,
  limits: { fileSize: 1000000 },
  fileFilter: function (req, file, cb) {

    console.log(JSON.stringify(file))

    checkTextFileType(file, cb);
  }
}).single('tb');

// Check File Type
function checkTextFileType(file, cb) {
  // Allowed ext
  const filetypes = /txt|json|js|csv/;
  // Check ext
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  // Check mime
  const mimetype = filetypes.test(file.mimetype);

  if (mimetype && extname) {
    return cb(null, true);
  } else {
    cb('Error: text, json, js or csvs Only!');
  }
}


// @route   POST api/upload/jsonfile
// @desc    Uploads one json file
// @access  Private
//router.get('/', (req, res) => res.render('index'));  // this renders ejs file

router.post('/jsonfile', (req, res) => {

  upljson(req, res, (err) => {
    if (err) {
      res.render('index', {
        msg: err
      });
    } else {
      if (req.file == undefined) {
        res.render('index', {
          msg: 'Error: No File Selected!'
        });
      } else {
        /*         res.render('index', {
                  // res.end({
                  msg: 'File Uploaded!',
                  file: `uploads/jsonfiles/${req.file.filename}`
                }); */

        res.json({
          msg: 'File Uploaded!',
          file: `uploads/jsonfiles/${req.file.filename}`
        });
      }
    }
  });
});




/*+++++  start PDF file upload */

// Set The Storage Engine
const storagePdfFiles = multer.diskStorage({
  destination: './public/uploads/pdffiles',
  filename: function (req, file, cb) {
    // cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    cb(null, file.originalname);
  }
});

// Init json file Upload
const uplpdf = multer({
  storage: storagePdfFiles,
  limits: { fileSize: 1000000 },
  fileFilter: function (req, file, cb) {

    console.log(JSON.stringify(file))

    checkTextFileType(file, cb);
  }
}).single('pdf');

// Check File Type
function checkTextFileType(file, cb) {
  // Allowed ext
  const filetypes = /pdf/;
  // Check ext
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  // Check mime
  const mimetype = filetypes.test(file.mimetype);

  if (mimetype && extname) {
    return cb(null, true);
  } else {
    cb('Error: not a pdf file!');
  }
}


// @route   POST api/upload/pdffile
// @desc    Uploads one json file
// @access  Private
//router.get('/', (req, res) => res.render('index'));  // this renders ejs file

let serverurl = "http://localhost:3001"


router.post('/pdffile', (req, res) => {
  uplpdf(req, res, (err) => {
    if (err) {
      res.render('index1', {
        msg: err
      });
    } else {
      if (req.file == undefined) {
        res.render('index1', {
          msg: 'Error: No File Selected!'
        });
      } else {

        let jlink = `${serverurl}/uploads/pdffiles/${req.file.filename}`
        res.render('index1', {
          // res.end({
          msg: 'File Uploaded!',
          file: `uploads/pdffiles/${req.file.filename}`,
          link: jlink
        });

        /*         res.json({
                  msg: 'File Uploaded!',
                  file: `uploads/pdffiles/${req.file.filename}`
                }); */
      }
    }
  });
});


// would like a set of routes to upload random files such as office docs, 

// i would like to upload multiple filese for each folder above such asimages, json files, csv files and pdf files and random files

router.post('/uploadmultiple', upload.array('myFiles', 12), (req, res, next) => {
  const files = req.files
  if (!files) {
    const error = new Error('Please choose files')
    error.httpStatusCode = 400
    return next(error)
  }

  res.send(files)

})

module.exports = router;