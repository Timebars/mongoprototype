// @desc    Logs request to console


/*

I have installed 'logger' module which do a better job than this logger middleware

*/


const logger = (req, res, next) => {
  console.log(
    `${req.method} ${req.protocol}://${req.get('host')}${req.originalUrl}`
  );
  next();
};

module.exports = logger;
