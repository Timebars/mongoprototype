﻿// Copyright © 2018 Timebars Ltd.  All rights reserved.

// Node Side Scheduing Engine
// calculates forecast and actual dates, work and cost and total work per week for rescalc tables

// variables for core scheduling engine


/* schEngInput.forEach(function (item) {


    console.log(item.resWorkHrsPerDayF)

}); */

$('#setFormValues').on('click', function () {

    var seInput = {
        "tbID": "1",
        "apStatusDate": "12 Dec 20",

        "StartF": "12 Dec 20",
        "FinishF": "15 Dec 20",
        "durF": "3",
        "resWorkHrsPerDayF": "1",
        "pctAllocF": "2",
        "hrsF": "3",
        "remDurF": "4",
        "remHrsF": "5",
        "pctComplF": "6",
    }

    // coreSchedulingEngine(seInput)
    setForm(seInput)

});

$('#showFormValues').on('click', function () {

    var StartF = $("#fldStartF").text()
    var FinishF = $("#fldStartF").text()
    var durF = $("#fldStartF").text()
    var resWorkHrsPerDayF = $("#fldStartF").text()
    var pctAllocF = $("#fldStartF").text()
    var hrsF = $("#fldStartF").text()
    var remDurF = $("#fldStartF").text()
    var remHrsF = $("#fldStartF").text()
    var pctComplF = $("#fldStartF").text()


    console.log(`StartF form value ${StartF}`)
    console.log(`FinishF form value ${FinishF}`)
    console.log(`durF form value ${durF}`)
    console.log(`resWorkHrsPerDayF form value ${resWorkHrsPerDayF}`)
    console.log(`pctAllocF form value ${pctAllocF}`)
    console.log(`hrsF form value ${hrsF}`)
    console.log(`remDurF form value ${remDurF}`)
    console.log(`remHrsF form value ${remHrsF}`)
    console.log(`pctComplF form value ${pctComplF}`)

});



function setForm(seInput) {

    // popup alloc form set values
    $("#fldStartF").text(seInput.StartF);
    $("#fldFinishF").text(seInput.FinishF);
    $("#fldDurF").text(seInput.durF);
    $("#fldResWorkHrsPerDayF").text(seInput.resWorkHrsPerDayF);
    $("#fldPctAllocF").text(seInput.pctAllocF);
    $("#fldHrsF").text(seInput.hrsF);
    $("#fldRemDurF").text(seInput.remDurF);
    $("#fldRemHrsF").text(seInput.remHrsF);
    $("#fldPctComplF").text(seInput.pctComplF);

    // actuals (pctComplA * 100) + "%"
    /*     $("#fldStartA").text(startA);
        $("#fldFinishA").text(finishA);
        $("#fldDurA").text(durA);
        $("#fldResWorkHrsPerDayA").text(resWorkHrsPerDayA);
        $("#fldPctAllocA").text(pctAllocA);
        $("#fldHrsA").text(hrsA);
        $("#fldRemDurA").text(remDurA);
        $("#fldRemHrsA").text(remHrsA); // hrs to go
        $("#fldPctComplA").text(pctComplA); */

}







function coreSchedulingEngine(seInput) {

    console.log("jim 5445")

    console.log(seInput.dur)

    var tbID = seInput.tbID
    var apStatusDate = seInput.apStatusDate
    var resWorkHrsPerDay = seInput.resWorkHrsPerDay
    var tbPayRate = seInput.tbPayRate

    // variables for core scheduling engine
    var startF = ""
    var startA = ""

    var finishF = ""
    var finishA = ""

    var durF = 0
    var durA = 0
    var dur = 0

    var resWorkHrsPerDayF = 0; //tbCalendar
    var resWorkHrsPerDayA = 0;

    var pctAllocF = 0  // tbPercentTimeOn
    var pctAllocA = 0;

    var hrsF = 0
    var hrsA = 0

    var cstF = 0
    var cstA = 0

    var remDurF = 0
    var remDurA = 0
    var remDur = 0

    var remHrsF = 0
    var remHrsA = 0
    var remHrs = 0

    var remCstF = 0
    var remCstA = 0
    var remCst = 0

    var pctComplF = 0
    var pctComplA = 0
    var pctCompl = 0


    //  below are three scenarios, task is in progresss (started not finished), task is completed (100% finished) or task is not started (in future) ()

    // task is in progress (started not finished)
    if ((dStart - statusDate < 0) && (dFinish - statusDate > 0)) {

        startF = moment(dStart).format("D MMM YYYY")
        startA = moment(dStart).format("D MMM YYYY")
        startFOnForm = "na"

        finishF = moment(dFinish).format("D MMM YYYY")
        finishA = "na"
        finishFOnForm = finishF

        durF = Number(getWorkingDays(dStart, dFinish))
        durA = Number(getWorkingDays(dStart, statusDate))
        dur = durF

        remDurF = "na"
        remDurA = durF - durA
        remDur = durF - durA

        pctAllocF = pctAlloc // get     from idb see above
        pctAllocA = pctAlloc
        // calendar
        resWorkHrsPerDayF = resWorkHrsPerDay // get from idb see above
        resWorkHrsPerDayA = resWorkHrsPerDay

        // %%%%%%%%%%% start alloc calcs, consider slider over-rides
        hrsF = resWorkHrsPerDayF * (pctAllocF / 100) * durF;
        cstF = hrsF * tbPayRate
        hrsF = parseFloat((hrsF).toFixed(1))

        hrsA = resWorkHrsPerDayA * (pctAllocA / 100) * durA
        cstA = hrsA * tbPayRate
        hrsA = parseFloat((hrsA).toFixed(1))

        remHrsF = "na"
        remHrsA = resWorkHrsPerDayA * (pctAllocA / 100) * remDurA
        remCst = remHrsA * tbPayRate

        remHrsA = parseFloat((remHrsA).toFixed(1))
        remHrs = remHrsA

        pctComplF = "na"
        pctComplA = (hrsF - remHrsA) / hrsF * 100
        pctComplA = parseFloat((pctComplA).toFixed(1)) //+ "%"
        pctCompl = pctComplA
        if (isNaN(parseFloat(pctCompl))) {
            pctCompl = 0
        }

        //%%%%%%%%%%% end alloc calcs, considering slider over-rides

        // task is completed 
    } else if ((dStart - statusDate < 0) && (dFinish - statusDate < 0)) {
        //console.log("dStart and finish before status date: " + (dStart - statusDate) / 1000 / 60 / 60 / 24)
        startF = moment(dStart).format("D MMM YYYY")
        startA = moment(dStart).format("D MMM YYYY")
        startFOnForm = "na"

        finishF = moment(dFinish).format("D MMM YYYY")
        finishA = moment(dFinish).format("D MMM YYYY")
        finishFOnForm = "na"

        durF = getWorkingDays(dStart, dFinish)
        durA = durF // when task is done, forcast and actual dur are equal
        dur = durF

        remDurF = 0
        remDurA = 0
        remDur = 0

        pctAllocF = pctAlloc // the next 4, get from idb see above, so what is was is what it will be
        pctAllocA = pctAlloc
        resWorkHrsPerDayF = resWorkHrsPerDay // get from idb see above
        resWorkHrsPerDayA = resWorkHrsPerDay

        hrsF = 0
        cstF = 0 * tbPayRate
        hrsA = resWorkHrsPerDayA * (pctAllocA / 100) * durA

        cstA = hrsA * Number(tbPayRate)



        hrsA = parseFloat((hrsA).toFixed(1))

        remHrsF = 0
        remHrsA = 0 //resWorkHrsPerDayA * (pctAllocA / 100) * remDurA
        remHrsA = 0 //parseFloat((remHrsA).toFixed(1))
        remHrs = 0

        remCst = 0 * tbPayRate

        pctComplF = 0
        pctComplA = 100 //(hrsA - remHrsA) / hrsA * 100
        //pctComplA = parseFloat((pctComplA).toFixed(1)) //+ "%"
        pctCompl = 100


        // task is not started / in future, no A Start
    } else if (dStart - statusDate > 0) {

        startF = moment(dStart).format("D MMM YYYY")
        startA = ""
        startFOnForm = startF
        //console.log("buildAllocStatusForm startFOnForm: " + startFOnForm)

        finishF = moment(dFinish).format("D MMM YYYY")
        finishA = ""
        finishFOnForm = finishF

        durF = getWorkingDays(dStart, dFinish)
        durA = ""
        dur = durF

        remDurF = ""
        remDurA = ""
        remDur = durF

        pctAllocF = pctAlloc // get from idb see above
        pctAllocA = pctAlloc

        resWorkHrsPerDayF = resWorkHrsPerDay // get from idb see above
        resWorkHrsPerDayA = resWorkHrsPerDay

        hrsF = resWorkHrsPerDayF * (pctAllocF / 100) * durF;
        cstF = hrsF * tbPayRate
        hrsF = parseFloat((hrsF).toFixed(1))
        hrsA = ""

        remHrsF = ""
        remHrsA = ""
        remHrs = hrsF


        cstA = 0 * tbPayRate
        remCst = remHrs * tbPayRate

        pctComplF = ""
        pctComplA = ""
        pctCompl = ""
        // #### #### #### scheduling algorithim  is done, now determine when to update idb or form


    };
}
// end core sch engine

module.exports = coreSchedulingEngine;

/*     var schEngInput = {

        "tbID": "1",
        "apStatusDate" : "12 Dec 20",
        "tbPayRate" : "50",
        "resWorkHrsPerDay" : "7.5",

        "dStart" : "12 Dec 20",
        "dFinish" : "12 Dec 20",

        "durF" : "0",
        "durA" : "0",
        "dur" : "0",



        "pctAllocF" : "0",
        "pctAllocA" : "0",

        "hrsF" : "0",
        "hrsA" : "0",

        "cstF" : "0",
        "cstA" : "0",

        "remDurF" : "0",
        "remDurA" : "0",
        "remDur" : "0",

        "remHrsF" : "0",
        "remHrsA" : "0",
        "remHrs" : "0",

        "remCstF" : "0",
        "remCstA" : "0",
        "remCst" : "0",

        "pctComplF" : "0",
        "pctComplA" : "0",
        "pctCompl" : "0",

    };
   */